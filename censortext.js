/**
 * Created by iqbal on 27/8/15.
 */
var censorWords= ["sad","bad","mad"]
var customCensorWords=[];

function censor(inStr){
    for(idx in censorWords){
        inStr=inStr.replace(censorWords[idx],"****");
    }
    for(idx in customCensorWords){
        inStr= inStr.replace(customCensorWords[idx],"****");
    }
    return inStr;
}

function addCensoreWord(word){
    customCensorWords.push(word);
}

function getCensoreWords(){
    return censorWords.concat(customCensorWords);
}
exports.censor=censor;
exports.addCensoreWord=addCensoreWord;
exports.getCensoreWords=getCensoreWords;